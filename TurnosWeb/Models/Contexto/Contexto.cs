﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.Contexto
{
    class ContextoClinica:DbContext
    {
        public ContextoClinica():base("ClinicaCS")
        {

        }
        public virtual DbSet<Paciente> Pacientes { get; set; }
        public virtual DbSet<Turno> Turnos { get; set; }
        public virtual DbSet<Medico> Medicos { get; set; }
        public virtual DbSet<ObraSocial> ObrasSociales { get; set; }
        public virtual DbSet<Horario> Horarios { get; set; }
        public virtual DbSet<Especialidad> Especialidades { get; set; }

    }
}

﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurnosWeb.Models.Enum;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Paciente")]
    [MetadataType(typeof(IPaciente))]
    public class Paciente : IdentityUser, IPaciente
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Direccion { get; set; }
        public string Dni { get; set; }
        public string Mail { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public bool EstaBorrado { get; set; }


        [EnumDataType(typeof(EstadoCivil))]
        public EstadoCivil EstadoCivil { get; set; }
        [EnumDataType(typeof(GrupoSanguineo))]
        public GrupoSanguineo GrupoSanguineo { get; set; }
        [EnumDataType(typeof(Sexo))]
        public Sexo Sexo { get; set; }

        public ICollection<ObraSocial> ObrasSociales { get; set; }
        public ICollection<Turno> Turnos { get; set; }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TurnosWeb.Startup))]
namespace TurnosWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
